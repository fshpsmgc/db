const {app, BrowserWindow} = require('electron')
const sqlite3 = require('sqlite3')

function createWindow(){
    const window = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    })

    window.loadFile('index.html')
}

app.whenReady().then(createWindow)
