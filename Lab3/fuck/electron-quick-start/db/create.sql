CREATE TABLE positions(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL,
  salary DECIMAL(7,2) NOT NULL
);

CREATE TABLE illness (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name varchar(30) NOT NULL,
  severity varchar(30) NOT NULL
);

CREATE TABLE wards (
  id INTEGER PRIMARY KEY AUTOINCREMENT
);

CREATE TABLE doctors(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL,
  positionID INTEGER,
  FOREIGN KEY(positionID) REFERENCES positions(id)
);
----
CREATE TABLE offices (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  doctorID INTEGER NULL,
  FOREIGN KEY(doctorID) REFERENCES doctors(id)
);

CREATE TABLE patients (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name varchar(50) DEFAULT NULL,
  wardID INTEGER DEFAULT NULL,
  illnessID INTEGER DEFAULT NULL,
  doctorID INTEGER DEFAULT NULL,
  FOREIGN KEY(wardID) REFERENCES wards(id),
  FOREIGN KEY(illnessID) REFERENCES illness(id),
  FOREIGN KEY(doctorID) REFERENCES doctors(id)
);

CREATE TABLE patient_illness (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  patientID INTEGER DEFAULT NULL,
  illnessID INTEGER DEFAULT NULL,
  FOREIGN KEY(patientID) REFERENCES patients(id),
  FOREIGN KEY(illnessID) REFERENCES illness(id)
);

CREATE TABLE treatments (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  length time DEFAULT NULL,
  appointment time NOT NULL,
  patientID INTEGER DEFAULT NULL,
  doctorID INTEGER DEFAULT NULL,
  FOREIGN KEY(patientID) REFERENCES patients(id),
  FOREIGN KEY(doctorID) REFERENCES doctors(id)
);

------------

--CREATE TABLE `doctors` (
--  `id` INTEGER NOT NULL,
--  `name` varchar(50) DEFAULT NULL,
--  `positionID` INTEGER DEFAULT NULL
--);
--
--CREATE TABLE `illness` (
--  `id` INTEGER NOT NULL,
--  `name` varchar(30) NOT NULL,
--  `severity` varchar(30) NOT NULL
--);
--
--CREATE TABLE `offices` (
--  `id` INTEGER NOT NULL,
--  `doctorID` INTEGER DEFAULT NULL
--);
--
--CREATE TABLE `patient` (
--  `id` INTEGER NOT NULL,
--  `name` varchar(50) DEFAULT NULL,
--  `wardID` INTEGER DEFAULT NULL,
--  `illnessID` INTEGER DEFAULT NULL,
--  `doctorID` INTEGER DEFAULT NULL
--);
--
--CREATE TABLE `patient_illness` (
--  `id` INTEGER NOT NULL,
--  `patientID` INTEGER DEFAULT NULL,
--  `illnessID` INTEGER DEFAULT NULL
--);
--
--CREATE TABLE `positions` (
--  `id` INTEGER NOT NULL,
--  `name` varchar(50) DEFAULT NULL,
--  `salary` decimal(7,2) DEFAULT NULL
--);
--
--CREATE TABLE `treatments` (
--  `id` INTEGER NOT NULL,
--  `length` time DEFAULT NULL,
--  `appointment` time NOT NULL,
--  `patientID` INTEGER DEFAULT NULL,
--  `doctorID` INTEGER DEFAULT NULL
--);
--
--CREATE TABLE `wards` (
--  `id` INTEGER NOT NULL
--);
--
--ALTER TABLE `doctors`
--  ADD PRIMARY KEY (`id`),
--  ADD KEY `positionID` (`positionID`);
--
--ALTER TABLE `illness`
--  ADD PRIMARY KEY (`id`);
--
--ALTER TABLE `offices`
--  ADD PRIMARY KEY (`id`),
--  ADD KEY `doctorID` (`doctorID`);
--
--ALTER TABLE `patient`
--  ADD PRIMARY KEY (`id`),
--  ADD KEY `wardID` (`wardID`),
--  ADD KEY `illnessID` (`illnessID`),
--  ADD KEY `doctorID` (`doctorID`);
--
--ALTER TABLE `patient_illness`
--  ADD PRIMARY KEY (`id`),
--  ADD KEY `patientID` (`patientID`),
--  ADD KEY `illnessID` (`illnessID`);
--
--ALTER TABLE `positions`
--  ADD PRIMARY KEY (`id`);
--
--ALTER TABLE `treatments`
--  ADD PRIMARY KEY (`id`),
--  ADD KEY `patientID` (`patientID`),
--  ADD KEY `doctorID` (`doctorID`);
--
--ALTER TABLE `wards`
--  ADD PRIMARY KEY (`id`);