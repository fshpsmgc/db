const sqlite3 = require('sqlite3')
var output 

let db = new sqlite3.Database('db/asylum.db', (err)=>{
    if(err){
        return console.error(err.message);
    }
    console.log('Connected to the local SQLite db');
});


function selectAllDoctors(){
    var inputField = document.getElementById('nameInput')
    var query = 'SELECT * FROM doctors';
    output = document.getElementById('output');

    clearText()

    if(inputField.value.length != 0){
        query += ' WHERE name = \'' + inputField.value + '\';';
    }

    console.log(query)
    console.log(inputField.value)

    db.each(query, (err, row) => {
        if(err){
            throw err;
        }

        //output.innerHTML(row.id + " " + row.name + "<>")
        output.textContent += row.id + " " + row.name + "\n"; 
    })
}

function clearText(){
    output.textContent = "";
}