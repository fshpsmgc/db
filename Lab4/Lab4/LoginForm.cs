﻿using db_starships_server;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Data.SqlClient;

namespace Lab4 {
    public partial class LoginForm : Form {
        public LoginForm() {
            InitializeComponent();
        }

        public void Register(int isDoctor) {
            DBController.ExecuteNonQuery($"insert into users(name, password, is_doctor) values (\"{inputLogin.Text}\", \"{DBController.ComputeHash(inputPass.Text)}\", {isDoctor})");
        }
         
        public bool Login(bool isDoctor) {
            SQLiteConnection connection = DBController.ConnectToDatabase();
            string query = $"select * from users where name = \"{inputLogin.Text}\"";

            if (isDoctor) {
                query += $" and is_doctor = {Convert.ToInt32(isDoctor)}";
            }

            SQLiteDataReader dataReader = DBController.ExecuteQuery(query, connection);

            if (dataReader == null) {
                MessageBox.Show("Не удалось получить информацию о пользователях");
                return false;
            }

            if (!dataReader.HasRows) {
                MessageBox.Show($"Пользователь не найден\nselect * from users where name = \"{inputLogin.Text}\" and is_doctor = {Convert.ToInt32(isDoctor)}");
                return false;
            }

            dataReader.Read();

            if (DBController.ComputeHash(inputPass.Text) == dataReader.GetString(2)) {
                MainForm form = new MainForm(isDoctor, this);
                form.Show();
                Hide();
            }

            return true;
        }

        private void register_Click(object sender, EventArgs e) {
            int isDoctor = 0;

            DialogResult result = MessageBox.Show("Зарегистрировать как доктора?", "Регистрация", MessageBoxButtons.YesNoCancel);
            switch (result) {
                case DialogResult.Cancel:
                    return;
                case DialogResult.Yes:
                    isDoctor = 1;
                    break;
                case DialogResult.No:
                    isDoctor = 0;
                    break;
            }

            Register(isDoctor);
        }

        private void loginAsPatient_Click(object sender, EventArgs e) {
            Login(false);
        }

        private void loginAsDoctor_Click(object sender, EventArgs e) {
            Login(true);
        }
    }
}
