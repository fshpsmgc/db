﻿namespace Lab4 {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.addDoctorPanel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.addDoctorButton = new System.Windows.Forms.Button();
            this.positionListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.doctorsDGV = new System.Windows.Forms.DataGridView();
            this.doctorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.position = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.treatmentsDGV = new System.Windows.Forms.DataGridView();
            this.treatmentDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.treatmentLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.treatmentPatient = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.treatmentDoctor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.patientsDGV = new System.Windows.Forms.DataGridView();
            this.patientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.patientIllness = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.patientIllnessSeverity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assignedDoctor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.illnessDGV = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.severity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.officesDGV = new System.Windows.Forms.DataGridView();
            this.officeNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.officeDoctor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addTreatmentPanel = new System.Windows.Forms.Panel();
            this.addPatientPanel = new System.Windows.Forms.Panel();
            this.addIllnessPanel = new System.Windows.Forms.Panel();
            this.addOfficePanel = new System.Windows.Forms.Panel();
            this.addTreatmentDate = new System.Windows.Forms.DateTimePicker();
            this.treatmentDuration = new System.Windows.Forms.NumericUpDown();
            this.addTreatmentPatientList = new System.Windows.Forms.ListBox();
            this.addTreatmentDoctorsList = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.addPatientIllnessList = new System.Windows.Forms.ListBox();
            this.addPatientDoctorsList = new System.Windows.Forms.ListBox();
            this.addPatientName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.illnessName = new System.Windows.Forms.TextBox();
            this.illnessSeverity = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.officeDoctorList = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.addTreatmentButton = new System.Windows.Forms.Button();
            this.addPatient = new System.Windows.Forms.Button();
            this.addIllnessButton = new System.Windows.Forms.Button();
            this.addOfficeButton = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.addDoctorPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.doctorsDGV)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treatmentsDGV)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patientsDGV)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.illnessDGV)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.officesDGV)).BeginInit();
            this.addTreatmentPanel.SuspendLayout();
            this.addPatientPanel.SuspendLayout();
            this.addIllnessPanel.SuspendLayout();
            this.addOfficePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treatmentDuration)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(776, 426);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.addDoctorPanel);
            this.tabPage1.Controls.Add(this.doctorsDGV);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(768, 400);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Врачи";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // addDoctorPanel
            // 
            this.addDoctorPanel.Controls.Add(this.label2);
            this.addDoctorPanel.Controls.Add(this.addDoctorButton);
            this.addDoctorPanel.Controls.Add(this.positionListBox);
            this.addDoctorPanel.Controls.Add(this.label1);
            this.addDoctorPanel.Controls.Add(this.textBox1);
            this.addDoctorPanel.Location = new System.Drawing.Point(360, 6);
            this.addDoctorPanel.Name = "addDoctorPanel";
            this.addDoctorPanel.Size = new System.Drawing.Size(402, 388);
            this.addDoctorPanel.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Должность";
            // 
            // addDoctorButton
            // 
            this.addDoctorButton.Location = new System.Drawing.Point(87, 144);
            this.addDoctorButton.Name = "addDoctorButton";
            this.addDoctorButton.Size = new System.Drawing.Size(120, 23);
            this.addDoctorButton.TabIndex = 3;
            this.addDoctorButton.Text = "Добавить";
            this.addDoctorButton.UseVisualStyleBackColor = true;
            // 
            // positionListBox
            // 
            this.positionListBox.FormattingEnabled = true;
            this.positionListBox.Location = new System.Drawing.Point(87, 43);
            this.positionListBox.Name = "positionListBox";
            this.positionListBox.Size = new System.Drawing.Size(120, 95);
            this.positionListBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Имя";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(87, 17);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(120, 20);
            this.textBox1.TabIndex = 0;
            // 
            // doctorsDGV
            // 
            this.doctorsDGV.AllowUserToDeleteRows = false;
            this.doctorsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.doctorsDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.doctorName,
            this.position,
            this.salary});
            this.doctorsDGV.Location = new System.Drawing.Point(6, 6);
            this.doctorsDGV.Name = "doctorsDGV";
            this.doctorsDGV.Size = new System.Drawing.Size(348, 388);
            this.doctorsDGV.TabIndex = 0;
            // 
            // doctorName
            // 
            this.doctorName.HeaderText = "Имя";
            this.doctorName.Name = "doctorName";
            // 
            // position
            // 
            this.position.HeaderText = "Должность";
            this.position.Name = "position";
            // 
            // salary
            // 
            this.salary.HeaderText = "Зарплата";
            this.salary.Name = "salary";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.addTreatmentPanel);
            this.tabPage2.Controls.Add(this.treatmentsDGV);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(768, 400);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Процедуры";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // treatmentsDGV
            // 
            this.treatmentsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.treatmentsDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.treatmentDate,
            this.treatmentLength,
            this.treatmentPatient,
            this.treatmentDoctor});
            this.treatmentsDGV.Location = new System.Drawing.Point(6, 6);
            this.treatmentsDGV.Name = "treatmentsDGV";
            this.treatmentsDGV.Size = new System.Drawing.Size(442, 388);
            this.treatmentsDGV.TabIndex = 1;
            // 
            // treatmentDate
            // 
            this.treatmentDate.HeaderText = "Время";
            this.treatmentDate.Name = "treatmentDate";
            // 
            // treatmentLength
            // 
            this.treatmentLength.HeaderText = "Длительность";
            this.treatmentLength.Name = "treatmentLength";
            // 
            // treatmentPatient
            // 
            this.treatmentPatient.HeaderText = "Пациент";
            this.treatmentPatient.Name = "treatmentPatient";
            // 
            // treatmentDoctor
            // 
            this.treatmentDoctor.HeaderText = "Врач";
            this.treatmentDoctor.Name = "treatmentDoctor";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.addPatientPanel);
            this.tabPage3.Controls.Add(this.patientsDGV);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(768, 400);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Пациенты";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // patientsDGV
            // 
            this.patientsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.patientsDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.patientName,
            this.patientIllness,
            this.patientIllnessSeverity,
            this.assignedDoctor});
            this.patientsDGV.Location = new System.Drawing.Point(6, 6);
            this.patientsDGV.Name = "patientsDGV";
            this.patientsDGV.Size = new System.Drawing.Size(442, 388);
            this.patientsDGV.TabIndex = 2;
            // 
            // patientName
            // 
            this.patientName.HeaderText = "Имя";
            this.patientName.Name = "patientName";
            // 
            // patientIllness
            // 
            this.patientIllness.HeaderText = "Болезнь";
            this.patientIllness.Name = "patientIllness";
            // 
            // patientIllnessSeverity
            // 
            this.patientIllnessSeverity.HeaderText = "Тяжесть";
            this.patientIllnessSeverity.Name = "patientIllnessSeverity";
            // 
            // assignedDoctor
            // 
            this.assignedDoctor.HeaderText = "Лечащий врач";
            this.assignedDoctor.Name = "assignedDoctor";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.addIllnessPanel);
            this.tabPage4.Controls.Add(this.illnessDGV);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(768, 400);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Болезни";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // illnessDGV
            // 
            this.illnessDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.illnessDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.severity});
            this.illnessDGV.Location = new System.Drawing.Point(6, 6);
            this.illnessDGV.Name = "illnessDGV";
            this.illnessDGV.Size = new System.Drawing.Size(238, 388);
            this.illnessDGV.TabIndex = 2;
            // 
            // name
            // 
            this.name.HeaderText = "Название";
            this.name.Name = "name";
            // 
            // severity
            // 
            this.severity.HeaderText = "Тяжесть";
            this.severity.Name = "severity";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.addOfficePanel);
            this.tabPage5.Controls.Add(this.officesDGV);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(768, 400);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Кабинеты";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // officesDGV
            // 
            this.officesDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.officesDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.officeNumber,
            this.officeDoctor});
            this.officesDGV.Location = new System.Drawing.Point(6, 6);
            this.officesDGV.Name = "officesDGV";
            this.officesDGV.Size = new System.Drawing.Size(240, 388);
            this.officesDGV.TabIndex = 2;
            // 
            // officeNumber
            // 
            this.officeNumber.HeaderText = "Номер";
            this.officeNumber.Name = "officeNumber";
            // 
            // officeDoctor
            // 
            this.officeDoctor.HeaderText = "Врач";
            this.officeDoctor.Name = "officeDoctor";
            // 
            // addTreatmentPanel
            // 
            this.addTreatmentPanel.Controls.Add(this.addTreatmentButton);
            this.addTreatmentPanel.Controls.Add(this.label6);
            this.addTreatmentPanel.Controls.Add(this.label5);
            this.addTreatmentPanel.Controls.Add(this.label4);
            this.addTreatmentPanel.Controls.Add(this.label3);
            this.addTreatmentPanel.Controls.Add(this.addTreatmentDoctorsList);
            this.addTreatmentPanel.Controls.Add(this.addTreatmentPatientList);
            this.addTreatmentPanel.Controls.Add(this.treatmentDuration);
            this.addTreatmentPanel.Controls.Add(this.addTreatmentDate);
            this.addTreatmentPanel.Location = new System.Drawing.Point(454, 6);
            this.addTreatmentPanel.Name = "addTreatmentPanel";
            this.addTreatmentPanel.Size = new System.Drawing.Size(308, 388);
            this.addTreatmentPanel.TabIndex = 2;
            // 
            // addPatientPanel
            // 
            this.addPatientPanel.Controls.Add(this.addPatient);
            this.addPatientPanel.Controls.Add(this.label9);
            this.addPatientPanel.Controls.Add(this.label8);
            this.addPatientPanel.Controls.Add(this.addPatientName);
            this.addPatientPanel.Controls.Add(this.addPatientDoctorsList);
            this.addPatientPanel.Controls.Add(this.addPatientIllnessList);
            this.addPatientPanel.Controls.Add(this.label7);
            this.addPatientPanel.Location = new System.Drawing.Point(454, 6);
            this.addPatientPanel.Name = "addPatientPanel";
            this.addPatientPanel.Size = new System.Drawing.Size(311, 388);
            this.addPatientPanel.TabIndex = 3;
            // 
            // addIllnessPanel
            // 
            this.addIllnessPanel.Controls.Add(this.addIllnessButton);
            this.addIllnessPanel.Controls.Add(this.label11);
            this.addIllnessPanel.Controls.Add(this.label10);
            this.addIllnessPanel.Controls.Add(this.illnessSeverity);
            this.addIllnessPanel.Controls.Add(this.illnessName);
            this.addIllnessPanel.Location = new System.Drawing.Point(250, 6);
            this.addIllnessPanel.Name = "addIllnessPanel";
            this.addIllnessPanel.Size = new System.Drawing.Size(515, 388);
            this.addIllnessPanel.TabIndex = 3;
            // 
            // addOfficePanel
            // 
            this.addOfficePanel.Controls.Add(this.addOfficeButton);
            this.addOfficePanel.Controls.Add(this.label12);
            this.addOfficePanel.Controls.Add(this.officeDoctorList);
            this.addOfficePanel.Location = new System.Drawing.Point(252, 6);
            this.addOfficePanel.Name = "addOfficePanel";
            this.addOfficePanel.Size = new System.Drawing.Size(513, 388);
            this.addOfficePanel.TabIndex = 3;
            // 
            // addTreatmentDate
            // 
            this.addTreatmentDate.Location = new System.Drawing.Point(90, 3);
            this.addTreatmentDate.Name = "addTreatmentDate";
            this.addTreatmentDate.Size = new System.Drawing.Size(200, 20);
            this.addTreatmentDate.TabIndex = 0;
            // 
            // treatmentDuration
            // 
            this.treatmentDuration.Location = new System.Drawing.Point(90, 29);
            this.treatmentDuration.Name = "treatmentDuration";
            this.treatmentDuration.Size = new System.Drawing.Size(120, 20);
            this.treatmentDuration.TabIndex = 1;
            // 
            // addTreatmentPatientList
            // 
            this.addTreatmentPatientList.FormattingEnabled = true;
            this.addTreatmentPatientList.Location = new System.Drawing.Point(90, 55);
            this.addTreatmentPatientList.Name = "addTreatmentPatientList";
            this.addTreatmentPatientList.Size = new System.Drawing.Size(120, 95);
            this.addTreatmentPatientList.TabIndex = 2;
            // 
            // addTreatmentDoctorsList
            // 
            this.addTreatmentDoctorsList.FormattingEnabled = true;
            this.addTreatmentDoctorsList.Location = new System.Drawing.Point(90, 156);
            this.addTreatmentDoctorsList.Name = "addTreatmentDoctorsList";
            this.addTreatmentDoctorsList.Size = new System.Drawing.Size(120, 95);
            this.addTreatmentDoctorsList.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Время";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Длительность";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Пациент";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Врач";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Имя";
            // 
            // addPatientIllnessList
            // 
            this.addPatientIllnessList.FormattingEnabled = true;
            this.addPatientIllnessList.Location = new System.Drawing.Point(59, 29);
            this.addPatientIllnessList.Name = "addPatientIllnessList";
            this.addPatientIllnessList.Size = new System.Drawing.Size(120, 95);
            this.addPatientIllnessList.TabIndex = 1;
            // 
            // addPatientDoctorsList
            // 
            this.addPatientDoctorsList.FormattingEnabled = true;
            this.addPatientDoctorsList.Location = new System.Drawing.Point(59, 130);
            this.addPatientDoctorsList.Name = "addPatientDoctorsList";
            this.addPatientDoctorsList.Size = new System.Drawing.Size(120, 95);
            this.addPatientDoctorsList.TabIndex = 2;
            // 
            // addPatientName
            // 
            this.addPatientName.Location = new System.Drawing.Point(59, 3);
            this.addPatientName.Name = "addPatientName";
            this.addPatientName.Size = new System.Drawing.Size(120, 20);
            this.addPatientName.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Болезнь";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 130);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Врач";
            // 
            // illnessName
            // 
            this.illnessName.Location = new System.Drawing.Point(66, 3);
            this.illnessName.Name = "illnessName";
            this.illnessName.Size = new System.Drawing.Size(120, 20);
            this.illnessName.TabIndex = 0;
            // 
            // illnessSeverity
            // 
            this.illnessSeverity.Location = new System.Drawing.Point(66, 29);
            this.illnessSeverity.Name = "illnessSeverity";
            this.illnessSeverity.Size = new System.Drawing.Size(120, 20);
            this.illnessSeverity.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Название";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Тяжесть";
            // 
            // officeDoctorList
            // 
            this.officeDoctorList.FormattingEnabled = true;
            this.officeDoctorList.Location = new System.Drawing.Point(40, 3);
            this.officeDoctorList.Name = "officeDoctorList";
            this.officeDoctorList.Size = new System.Drawing.Size(120, 95);
            this.officeDoctorList.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Врач";
            // 
            // addTreatmentButton
            // 
            this.addTreatmentButton.Location = new System.Drawing.Point(90, 257);
            this.addTreatmentButton.Name = "addTreatmentButton";
            this.addTreatmentButton.Size = new System.Drawing.Size(120, 23);
            this.addTreatmentButton.TabIndex = 8;
            this.addTreatmentButton.Text = "Добавить";
            this.addTreatmentButton.UseVisualStyleBackColor = true;
            // 
            // addPatient
            // 
            this.addPatient.Location = new System.Drawing.Point(59, 231);
            this.addPatient.Name = "addPatient";
            this.addPatient.Size = new System.Drawing.Size(120, 23);
            this.addPatient.TabIndex = 6;
            this.addPatient.Text = "Добавить";
            this.addPatient.UseVisualStyleBackColor = true;
            // 
            // addIllnessButton
            // 
            this.addIllnessButton.Location = new System.Drawing.Point(66, 55);
            this.addIllnessButton.Name = "addIllnessButton";
            this.addIllnessButton.Size = new System.Drawing.Size(120, 23);
            this.addIllnessButton.TabIndex = 7;
            this.addIllnessButton.Text = "Добавить";
            this.addIllnessButton.UseVisualStyleBackColor = true;
            // 
            // addOfficeButton
            // 
            this.addOfficeButton.Location = new System.Drawing.Point(40, 104);
            this.addOfficeButton.Name = "addOfficeButton";
            this.addOfficeButton.Size = new System.Drawing.Size(120, 23);
            this.addOfficeButton.TabIndex = 7;
            this.addOfficeButton.Text = "Добавить";
            this.addOfficeButton.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.addDoctorPanel.ResumeLayout(false);
            this.addDoctorPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.doctorsDGV)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treatmentsDGV)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.patientsDGV)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.illnessDGV)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.officesDGV)).EndInit();
            this.addTreatmentPanel.ResumeLayout(false);
            this.addTreatmentPanel.PerformLayout();
            this.addPatientPanel.ResumeLayout(false);
            this.addPatientPanel.PerformLayout();
            this.addIllnessPanel.ResumeLayout(false);
            this.addIllnessPanel.PerformLayout();
            this.addOfficePanel.ResumeLayout(false);
            this.addOfficePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treatmentDuration)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView doctorsDGV;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView treatmentsDGV;
        private System.Windows.Forms.DataGridView patientsDGV;
        private System.Windows.Forms.DataGridView officesDGV;
        private System.Windows.Forms.DataGridView illnessDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn severity;
        private System.Windows.Forms.DataGridViewTextBoxColumn doctorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn position;
        private System.Windows.Forms.DataGridViewTextBoxColumn salary;
        private System.Windows.Forms.DataGridViewTextBoxColumn officeNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn officeDoctor;
        private System.Windows.Forms.Panel addDoctorPanel;
        private System.Windows.Forms.ListBox positionListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button addDoctorButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn treatmentDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn treatmentLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn treatmentPatient;
        private System.Windows.Forms.DataGridViewTextBoxColumn treatmentDoctor;
        private System.Windows.Forms.DataGridViewTextBoxColumn patientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn patientIllness;
        private System.Windows.Forms.DataGridViewTextBoxColumn patientIllnessSeverity;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignedDoctor;
        private System.Windows.Forms.Panel addTreatmentPanel;
        private System.Windows.Forms.Panel addPatientPanel;
        private System.Windows.Forms.Panel addIllnessPanel;
        private System.Windows.Forms.Panel addOfficePanel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox addTreatmentDoctorsList;
        private System.Windows.Forms.ListBox addTreatmentPatientList;
        private System.Windows.Forms.NumericUpDown treatmentDuration;
        private System.Windows.Forms.DateTimePicker addTreatmentDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox addPatientName;
        private System.Windows.Forms.ListBox addPatientDoctorsList;
        private System.Windows.Forms.ListBox addPatientIllnessList;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button addTreatmentButton;
        private System.Windows.Forms.Button addPatient;
        private System.Windows.Forms.Button addIllnessButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox illnessSeverity;
        private System.Windows.Forms.TextBox illnessName;
        private System.Windows.Forms.Button addOfficeButton;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListBox officeDoctorList;
    }
}