﻿namespace Lab4 {
    partial class LoginForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.inputLogin = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.inputPass = new System.Windows.Forms.TextBox();
            this.loginAsPatient = new System.Windows.Forms.Button();
            this.loginAsDoctor = new System.Windows.Forms.Button();
            this.register = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // inputLogin
            // 
            this.inputLogin.Location = new System.Drawing.Point(63, 6);
            this.inputLogin.Name = "inputLogin";
            this.inputLogin.Size = new System.Drawing.Size(100, 20);
            this.inputLogin.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Логин";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Пароль";
            // 
            // inputPass
            // 
            this.inputPass.Location = new System.Drawing.Point(63, 32);
            this.inputPass.Name = "inputPass";
            this.inputPass.PasswordChar = '*';
            this.inputPass.Size = new System.Drawing.Size(100, 20);
            this.inputPass.TabIndex = 2;
            // 
            // loginAsPatient
            // 
            this.loginAsPatient.Location = new System.Drawing.Point(12, 58);
            this.loginAsPatient.Name = "loginAsPatient";
            this.loginAsPatient.Size = new System.Drawing.Size(151, 23);
            this.loginAsPatient.TabIndex = 4;
            this.loginAsPatient.Text = "Войти как пациент";
            this.loginAsPatient.UseVisualStyleBackColor = true;
            this.loginAsPatient.Click += new System.EventHandler(this.loginAsPatient_Click);
            // 
            // loginAsDoctor
            // 
            this.loginAsDoctor.Location = new System.Drawing.Point(12, 87);
            this.loginAsDoctor.Name = "loginAsDoctor";
            this.loginAsDoctor.Size = new System.Drawing.Size(151, 23);
            this.loginAsDoctor.TabIndex = 5;
            this.loginAsDoctor.Text = "Войти как врач";
            this.loginAsDoctor.UseVisualStyleBackColor = true;
            this.loginAsDoctor.Click += new System.EventHandler(this.loginAsDoctor_Click);
            // 
            // register
            // 
            this.register.Location = new System.Drawing.Point(11, 116);
            this.register.Name = "register";
            this.register.Size = new System.Drawing.Size(151, 23);
            this.register.TabIndex = 6;
            this.register.Text = "Зарегистрировать";
            this.register.UseVisualStyleBackColor = true;
            this.register.Click += new System.EventHandler(this.register_Click);
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(174, 146);
            this.Controls.Add(this.register);
            this.Controls.Add(this.loginAsDoctor);
            this.Controls.Add(this.loginAsPatient);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.inputPass);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.inputLogin);
            this.Name = "LoginForm";
            this.Text = "Вход";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox inputLogin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox inputPass;
        private System.Windows.Forms.Button loginAsPatient;
        private System.Windows.Forms.Button loginAsDoctor;
        private System.Windows.Forms.Button register;
    }
}

