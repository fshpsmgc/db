﻿using db_starships_server;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Lab4 {
    public partial class MainForm : Form {
        bool isDoctor;
        LoginForm loginForm;
        public MainForm(bool _isDoctor, LoginForm _loginForm) {
            InitializeComponent();

            LoadDoctors();

            isDoctor = _isDoctor;
            if (isDoctor) {
                Text += ": [Доктор]";
                SetPanelState(true);
            } else {
                Text += ": [Пациент]";
                SetPanelState(false);
            }

            loginForm = _loginForm;
        }

        private void SetPanelState(bool state) {
            addDoctorPanel.Enabled = state;
            addIllnessPanel.Enabled = state;
            addOfficePanel.Enabled = state;
            addPatientPanel.Enabled = state;
            addTreatmentPanel.Enabled = state;
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e) {
            loginForm.Show();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e) {
            switch (tabControl1.SelectedIndex) {
                case 0:
                    LoadDoctors();
                    break;
                case 1:
                    LoadTreatments();
                    break;
                case 2:
                    LoadPatients();
                    break;
                case 3:
                    LoadIllnesses();
                    break;
                case 4:
                    LoadOffices();
                    break;
            }
        }

        private void LoadDoctors() {
            string query = "select doctors.name, positions.name, positions.salary from doctors inner join positions on positions.id = doctors.positionID;";

            SQLiteConnection connection = DBController.ConnectToDatabase();
            SQLiteDataReader data = DBController.ExecuteQuery(query, connection);

            doctorsDGV.Rows.Clear();

            while (data.Read()) {
                doctorsDGV.Rows.Add(data.GetString(0), data.GetString(1), data.GetInt32(2).ToString());
            }

            connection.Close();
            connection = DBController.ConnectToDatabase();

            query = "select name from positions";
            data = DBController.ExecuteQuery(query, connection);
            while (data.Read()) {
                positionListBox.Items.Add(data.GetString(0));
            }
        }

        private void LoadTreatments() {
            string query = "select treatments.appointment, treatments.length, patients.name, doctors.name  from " +
                            "((treatments inner join doctors on treatments.doctorID = doctors.id) " +
                            "inner join patients on treatments.patientID= patients.id);";

            SQLiteConnection connection = DBController.ConnectToDatabase();
            SQLiteDataReader data = DBController.ExecuteQuery(query, connection);

            treatmentsDGV.Rows.Clear();

            while (data.Read()) {
                treatmentsDGV.Rows.Add(UnixTimeStampToDateTime(data.GetInt32(0)).ToString(), data.GetInt32(1), data.GetString(2), data.GetString(3));
            }
        }

        public static DateTime UnixTimeStampToDateTime(int unixTimeStamp) {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        //TODO
        private void LoadPatients() {
            string query = "select patients.name, illness.name, illness.severity, doctors.name from ((patients" +
                " inner join illness on patients.illnessID = illness.id) " +
                "inner join doctors on patients.doctorID = doctors.id)";

            SQLiteConnection connection = DBController.ConnectToDatabase();
            SQLiteDataReader data = DBController.ExecuteQuery(query, connection);

            patientsDGV.Rows.Clear();

            while (data.Read()) {
                patientsDGV.Rows.Add(data.GetString(0), data.GetString(1), data.GetString(2), data.GetString(3));
            }
        }

        private void LoadIllnesses() {
            string query = "select * from illness";

            SQLiteConnection connection = DBController.ConnectToDatabase();
            SQLiteDataReader data = DBController.ExecuteQuery(query, connection);

            illnessDGV.Rows.Clear();

            while (data.Read()) {
                illnessDGV.Rows.Add(data.GetString(1), data.GetString(2));
            }
        }

        private void LoadOffices() {
            string query = "select offices.id, doctors.name  from offices inner join doctors on doctors.id = offices.doctorID;";

            SQLiteConnection connection = DBController.ConnectToDatabase();
            SQLiteDataReader data = DBController.ExecuteQuery(query, connection);

            officesDGV.Rows.Clear();

            while (data.Read()) {
                officesDGV.Rows.Add(data.GetInt32(0).ToString(), data.GetString(1));
            }
        }
    }
}
