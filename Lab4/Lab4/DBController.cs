﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Security.Cryptography;
using System.IO;


namespace db_starships_server {
    struct QueryParameter {
        public string parameter;
        public object value;

        public QueryParameter(string p, object v) {
            parameter = p;
            value = v;
        }
    }

    class DBController {
        public static string dbFilename = "asylum.db";
        
        static RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        
        /// <summary>
        /// Get crypto salt
        /// </summary>
        /// <param name="maxSaltLength"></param>
        /// <returns></returns>
        public static string GetSalt(int maxSaltLength = 32) {
            var salt = new byte[maxSaltLength];

            rng.GetNonZeroBytes(salt);

            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < salt.Length; i++) {
                builder.Append(salt[i].ToString("x2"));
            }
            return builder.ToString();
        }

        /// <summary>
        /// Return data hash
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        public static string ComputeHash(string rawData) {
            using (SHA256 sha256 = SHA256.Create()) {
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(rawData));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++) {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        /// <summary>
        /// Establish connection to database
        /// </summary>
        /// <returns></returns>
        public static SQLiteConnection ConnectToDatabase() {
            return new SQLiteConnection($"Data Source={dbFilename};Version=3;");
        }

        /// <summary>
        /// Check if database exists
        /// </summary>
        /// <returns></returns>
        public static bool Exists() {
            return File.Exists(dbFilename);
        }

        /// <summary>
        /// Execute SQL query that doesn't return values
        /// </summary>
        /// <param name="query"></param>
        public static void ExecuteNonQuery(string query) {
            using (SQLiteConnection connection = ConnectToDatabase()) {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand(query, connection)) {
                    try {
                        command.ExecuteNonQuery();
                    }
                    catch (SQLiteException e) {

                        connection.Close();
                        return;
                    }
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Execute SQL query and return DataReader
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public static SQLiteDataReader ExecuteQuery(string query, SQLiteConnection connection) {
            connection.Open();
            SQLiteCommand command = new SQLiteCommand(query, connection);
            SQLiteDataReader data = null;
            try {
                data = command.ExecuteReader();
            }catch(SQLiteException e) {
                return null;
            }

            return data;
        }
    }
}
